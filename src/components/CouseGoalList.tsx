import CourseGoal from './CourseGoal.tsx'
import { CourseGoal as CGoal } from '../App.tsx'
import InfoBox from './InfoBox.tsx'
import { ReactNode } from 'react'

type CourseGoalListProps = {
  goals: CGoal[]
  onDelete: (id: number) => void
}

export default function CourseGoalList({
  goals,
  onDelete,
}: Readonly<CourseGoalListProps>) {
  if (goals.length === 0) {
    return (
      <InfoBox mode="hint">
        You have no course goals yet. Start by adding some!
      </InfoBox>
    )
  }

  let warningBox: ReactNode

  if (goals.length >= 4) {
    warningBox = (
      <InfoBox mode="warning" severity="medium">
        You have too much goals. Don't put too much on your plate!
      </InfoBox>
    )
  }

  return (
    <>
      {warningBox}
      <ul>
        {goals.map((goal) => (
          <li key={goal.id}>
            <CourseGoal id={goal.id} title={goal.title} onDelete={onDelete}>
              <p>{goal.description}</p>
            </CourseGoal>
          </li>
        ))}
      </ul>
    </>
  )
}
